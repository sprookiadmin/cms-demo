<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Json;
use backend\Models\ApiKey;
use backend\Models\ApiKeyQuery;
use backend\models\Notification;
use backend\models\NotificationQuery;
use backend\models\BeaconGroup;
use backend\models\BeaconGroupQuery;
use backend\models\Countries;
use backend\models\CountriesQuery;
use backend\models\Cities;
use backend\models\CitiesQuery;
use backend\models\Malls;
use backend\models\MallsQuery;
use backend\models\Contacts;
use backend\models\ContactsQuery;
use backend\models\Address;
use backend\models\AddressQuery;
use backend\models\ContactType;
use backend\models\ContactTypeQuery;
use backend\models\Beacons;
use backend\models\BeaconsQuery;
use backend\models\BeaconManufacturer;
use backend\models\BeaconManufacturerQuery;
use backend\models\BeaconType;
use backend\models\BeaconTypeQuery;
use linslin\yii2\curl;

class ApiController extends Controller
{
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => Yii::$app->params['allowedDomains'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT'],
                    'Access-Control-Request-Headers' => ['X-Wsse'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $action = $request->get('action');
        $token = $request->get('token');
        if($token != Yii::$app->params['apiToken']) {
            echo Json::encode(array('status' => 'NOK', 'message' => 'Invalid Token'));
            die();
        }
        switch ($action) {
            case 'getApiKey':
                $this->getApiKey($request);
                break;
            case 'updateApiKey':
                $this->updateAPIKey($request);
                break;
            case 'getAllBeacon':
                $this->getAllBeacon($request);
                break;
            case 'addNotification':
                $this->addNotification($request);
                break;
            case 'updateNotification':
                $this->updateNotification($request);
                break;
            case 'getAllNotification':
                $this->getAllNotification($request);
                break;
            case 'getNotificationById':
                $this->getNotificationById($request);
                break;
            case 'getMobileNotification':
                $this->getMobileNotification($request);
                break;
            case 'addBeaconGroup':
                $this->addBeaconGroup($request);
                break;
            case 'getAllBeaconGroup':
                $this->getAllBeaconGroup($request);
                break;
            case 'updateBeaconGroup':
                $this->updateBeaconGroup($request);
                break;
            case 'getBeaconGroupById':
                $this->getBeaconGroupById($request);
                break;
            case 'getCountries':
                $this->getCountries();
                break;
            case 'getCities':
                $this->getCities();
                break;
            case 'uploadImages':
                $this->uploadImages($request);
                break;
            case 'addMalls':
                $this->addMalls($request);
                break;
            case 'getMalls':
                $this->getMalls();
                break;
            case 'getMallById':
                $this->getMallById($request);
                break;
            case 'editMalls':
                $this->editMalls($request);
                break;
            case 'getAllBeaconManufacturer':
                $this->getAllBeaconManufacturer();
                break;
            case 'getAllBeaconType':
                $this->getAllBeaconType();
                break;
            case 'getBeaconTypeByManufacturerId':
                $this->getBeaconTypeByManufacturerId($request);
                break;
            case 'addBeacon':
                $this->addBeacon($request);
                break;
            default:
                echo Json::encode(array('invigor' => 1));
                die();
        }
    }
    
    public function getApiKey($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $userId = $request->post('userId');
        $error = array();
        if(empty($userId)) {
            $error[] = 'userId';
        }
        if(!empty($error)) {
            echo Json::encode(array('status' => 'NOK','message' => 'userId cannot be empty.'));
            die();
        }
        $apiKeyModel = ApiKey::find()
            ->where(['user_id' => $userId])
            ->one();
        if(!empty($apiKeyModel)) {
            echo Json::encode(array('status' => 'OK','api_key' => $apiKeyModel->api_key,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','message' => 'No record found'));
            die();
        }
    }
    
    public function getAllNotification($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $userId = $request->post('userId');
        if(empty($userId)) {
            $error[] = 'userId';
        }
        if(!empty($error)) {
            echo Json::encode(array('status' => 'NOK','message' => 'userId cannot be empty.'));
            die();
        }
        $notificationModel = Notification::findAll(['user_id' => $userId]);
        if(!empty($notificationModel)) {
            echo Json::encode(array('status' => 'OK','data' => $notificationModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getNotificationById($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $notificationId = $request->post('notificationId');
        if(empty($notificationId)) {
            $error[] = 'notificationId';
        }
        if(!empty($error)) {
            echo Json::encode(array('status' => 'NOK','message' => 'notificationId cannot be empty.'));
            die();
        }
        $notificationModel = Notification::findOne(['id' => $notificationId]);
        if(!empty($notificationModel)) {
            echo Json::encode(array('status' => 'OK','data' => $notificationModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getBeaconGroupById($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $beaconGroupId = $request->post('beaconGroupId');
        if(empty($beaconGroupId)) {
            $error[] = 'beaconGroupId';
        }
        if(!empty($error)) {
            echo Json::encode(array('status' => 'NOK','message' => '$beaconGroupId cannot be empty.'));
            die();
        }
        $beaconGroupModel = BeaconGroup::findOne(['id' => $beaconGroupId]);
        if(!empty($beaconGroupModel)) {
            echo Json::encode(array('status' => 'OK','data' => $beaconGroupModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getMobileNotification($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $userId = $request->post('userId');
        $beaconGroupId = $request->post('beaconGroupId');
        $proximity = $request->post('proximity');
        $major = $request->post('major');
        $minor = $request->post('minor');
        
        if(empty($userId) || empty($beaconGroupId) || empty($proximity) || empty($major) || empty($minor)) {
            $error[] = 'notificationId';
        }
        if(!empty($error)) {
            echo Json::encode(array('status' => 'NOK','message' => 'Missing parameters.'));
            die();
        }
        $notificationModel = Notification::findOne(
            [
                'user_id' => $userId, 
                'beacon_group_id' => $beaconGroupId,
                'proximity' => $proximity,
                'major' => $major,
                'minor' => $minor,
            ]
        );
        if(!empty($notificationModel)) {
            echo Json::encode(array('status' => 'OK','data' => $notificationModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function addNotification($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $userId = $request->post('userId');
        $notification = $request->post('notification');
        $beaconId = $request->post('beacon_id');
        $beaconGroupId = $request->post('beacon_group_id');
        $proximity = $request->post('proximity');
        $major = $request->post('major');
        $minor = $request->post('minor');
        
        $notificationModel = new Notification();
        $notificationModel->user_id = $userId;
        $notificationModel->notification = $notification;
        $notificationModel->beacon_id = $beaconId;
        $notificationModel->beacon_group_id = $beaconGroupId;
        $notificationModel->proximity = $proximity;
        $notificationModel->major = (int)$major;
        $notificationModel->minor = (int)$minor;
        $notificationModel->created_at = date('U');
        $notificationModel->updated_at = date('U');
            
        $notificationModel->validate();
        $error = $notificationModel->errors;
        if(empty($error)) {
            $notificationModel->save();
            echo Json::encode(array('status' => 'OK', 'message' => 'Notification successfully added.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while adding notification.'));
            die();
        }
    }
    
    public function updateNotification($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $notificationId = $request->post('notificationId');
        $userId = $request->post('userId');
        $notification = $request->post('notification');
        $beaconId = $request->post('beacon_id');
        $beaconGroupId = $request->post('beaconGroupId');
        $proximity = $request->post('proximity');
        $major = $request->post('major');
        $minor = $request->post('minor');
        
        $notificationModel = Notification::find()
            ->where(['id' => $notificationId, 'user_id' => $userId])
            ->one();
        
        if(!empty($notificationModel)) {
            $notificationModel->notification = $notification;
            $notificationModel->beacon_id = $beaconId;
            $notificationModel->beacon_group_id = $beaconGroupId;
            $notificationModel->proximity = $proximity;
            $notificationModel->major = (int)$major;
            $notificationModel->minor = (int)$minor;
            $notificationModel->updated_at = date('U');
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'Notification not found.'));
            die();
        }
        
        $notificationModel->validate();
        $error = $notificationModel->errors;
        if(empty($error)) {
            $notificationModel->save();
            echo Json::encode(array('status' => 'OK', 'message' => 'Notification successfully updated.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while updating notification.'));
            die();
        }
    }
    
    public function getAllBeacon($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $userId = $request->post('userId');
        $apiKeyModel = ApiKey::find()
            ->where(['user_id' => $userId])
            ->one();
        $beaconApiUrl = 'https://api.kontakt.io/device?access=SUPERVISOR&maxResult=500&deviceType=BEACON';
        $curl = new curl\Curl();
        $curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
        $curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOption(CURLOPT_RETURNTRANSFER, true);
        $curl->setOption(CURLOPT_HTTPHEADER, array('Api-Key: '.$apiKeyModel->api_key, 'Accept: application/vnd.com.kontakt+json;version=10'));
        $beacon = $curl->get($beaconApiUrl);
        echo $beacon;
        die();
    }
    
    public function updateApiKey($request)
    {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $userId = $request->post('userId');
        $apiKey = $request->post('apiKey');
        $error = array();
        if(empty($userId)) {
            $error[] = 'userId';
        }
        if(empty($apiKey)) {
            $error[] = 'apiKey';
        }
        if(!empty($error)) {
            echo Json::encode(array('status' => 'NOK','message' => implode(',',$error).' cannot be empty.'));
            die();
        }
        
        $apiKeyModel = ApiKey::find()
            ->where(['user_id' => $userId])
            ->one();
        if(!empty($apiKeyModel)) {
            $apiKeyModel->api_key = $apiKey;
            $apiKeyModel->updated_at = date('U');
        } else {    
            $apiKeyModel = new ApiKey();
            $apiKeyModel->user_id = $userId;
            $apiKeyModel->api_key = $apiKey;
            $apiKeyModel->created_at = date('U');
            $apiKeyModel->updated_at = date('U');
        }
        
        $apiKeyModel->validate();
        $error = $apiKeyModel->errors;
        
        if(empty($error)) {
            $apiKeyModel->save();
            echo Json::encode(array('status' => 'OK', 'message' => 'API Key successfully updated.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while updating. API Key is not updated.'));
            die();
        }
    }
    
    public function addBeaconGroup($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $beaconGroupName = $request->post('beaconGroupName');
        
        $beaconGroupModel = BeaconGroup::find()
            ->where(['name' => $beaconGroupName])
            ->one();
        
        if(!empty($beaconGroupModel)) {
            echo Json::encode(array('status' => 'NOK', 'message' => 'Beacon Group already exist. Please choose another name.'));
            die();
        }
        
        $beaconGroupModel = new BeaconGroup();
        $beaconGroupModel->name = $beaconGroupName;
        $beaconGroupModel->created_at = date('U');
        $beaconGroupModel->updated_at = date('U');
            
        $beaconGroupModel->validate();
        $error = $beaconGroupModel->errors;
        if(empty($error)) {
            $beaconGroupModel->save();
            echo Json::encode(array('status' => 'OK', 'message' => 'Beacon Group successfully added.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while adding Beacon Group.'));
            die();
        }
    }
    
    public function getAllBeaconGroup($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $beaconGroupModel = BeaconGroup::find()->all();
        if(!empty($beaconGroupModel)) {
            echo Json::encode(array('status' => 'OK','data' => $beaconGroupModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function updateBeaconGroup($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $beaconGroupId = $request->post('beaconGroupId');
        $beaconGroupName = $request->post('beaconGroupName');
        
        $beaconGroupModel = BeaconGroup::find()
            ->where(['id' => $beaconGroupId])
            ->one();
        
        if(empty($beaconGroupModel)) {
            echo Json::encode(array('status' => 'NOK', 'message' => 'Beacon Group not exist.'));
            die();
        }
        
        $beaconGroupModel->name = $beaconGroupName;
        $beaconGroupModel->updated_at = date('U');
            
        $beaconGroupModel->validate();
        $error = $beaconGroupModel->errors;
        if(empty($error)) {
            $beaconGroupModel->save();
            echo Json::encode(array('status' => 'OK', 'message' => 'Beacon Group successfully updated.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while updating Beacon Group.'));
            die();
        }
    }
    
    public function getCountries() {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $countryModel = Countries::find()->orderBy(['country' => SORT_ASC])->all();
        if(!empty($countryModel)) {
            echo Json::encode(array('status' => 'OK','data' => $countryModel,'message' => 'Record found'));
            die();
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getCities() {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $cityModel = Cities::find()->orderBy(['city' => SORT_ASC])->all();
        if(!empty($cityModel)) {
            echo Json::encode(array('status' => 'OK','data' => $cityModel,'message' => 'Record found'));
            die();
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function uploadImages($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $createdFile = array();
        if(count($_FILES['filesToUpload']['name'])) {
            $i = 0;
            foreach ($_FILES['filesToUpload']['name'] as $file) {
                $basePath = Yii::getAlias('@frontend');
                $encodedFile = md5($file).'.jpg';
                $createdFile[] = $encodedFile;
                $imgPath = $basePath."/web/uploads/".$encodedFile;
                $fileName = $_FILES['filesToUpload']['tmp_name'][$i];
                move_uploaded_file($fileName, $imgPath);
                chmod( $imgPath , 0777 );
                $i++;
            }
            echo Json::encode(array('status' => 'OK','data' => ['file' => $createdFile],'message' => 'Upload successful'));
            die();
        }
        echo Json::encode(array('status' => 'NOK','data' => $file,'message' => 'Upload fail'));
        die();
    }
    
    public function addMalls($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $addressModel = new Address();
        $addressModel->country_id = $request->post('country');
        $addressModel->city_id = $request->post('city');
        $addressModel->unit_no = $request->post('unit_no');
        $addressModel->street = $request->post('street');
        $addressModel->postal_code = $request->post('postal');
        $addressModel->created_at = date('U');
        $addressModel->updated_at = date('U');
        $addressModel->save();
        $addressModel->validate();
        
        $mallsModel = new Malls();
        $mallsModel->app_id = Yii::$app->params['appId'];
        $mallsModel->address_id = $addressModel->id;
        $mallsModel->name = $request->post('name');
        $mallsModel->description = $request->post('description');
        $mallsModel->email = $request->post('email');
        $mallsModel->website = $request->post('website');
        $mallsModel->phone_no = $request->post('phone_no');
        $mallsModel->fax = $request->post('fax');
        $mallsModel->logo = $request->post('logo');
        $mallsModel->opening_hours = $request->post('opening_hours');
        $mallsModel->parking_hours = $request->post('parking_hours');
        $mallsModel->services = $request->post('services');
        $mallsModel->concierge = $request->post('concierge');
        $mallsModel->latitude = $request->post('latitude');
        $mallsModel->longitude = $request->post('longitude');
        $mallsModel->radius = $request->post('radius');
        $mallsModel->status = Malls::STATUS_ACTIVE;
        $mallsModel->created_at = date('U');
        $mallsModel->updated_at = date('U');
        $mallsModel->validate();
        $error = $mallsModel->errors;
        if(empty($error)) {
            $mallsModel->save();
            $contactsModel = new Contacts();
            $contactsModel->type_id = ContactType::TYPE_MALL;
            $contactsModel->link_id = $mallsModel->id;
            $contactsModel->given_name = $request->post('given_name');
            $contactsModel->family_name = $request->post('family_name');
            $contactsModel->email = $request->post('contact_email');
            $contactsModel->phone_no = $request->post('contact_phone');
            $contactsModel->created_at = date('U');
            $contactsModel->updated_at = date('U');
            $contactsModel->save();            
            echo Json::encode(array('status' => 'OK', 'message' => 'Malls successfully added.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while adding Malls.'));
            die();
        }
    }
    
    public function getMalls() {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $mallsModel = (new \yii\db\Query())
        ->select(['malls.id', 'malls.name', 'malls.latitude', 'malls.longitude', 'malls.phone_no', 'malls.status',
            "CONCAT(contacts.given_name, ' ', contacts.family_name) AS contact_name", 'contacts.email as contact_email', 'contacts.phone_no AS contact_phone',
            'address.street', 'address.postal_code', 'countries.country', 'cities.city'])
        ->from('malls')
        ->join('LEFT JOIN', 'contacts', 'contacts.link_id = malls.id AND type_id=1')
        ->join('LEFT JOIN', 'address', 'address.id = malls.address_id')
        ->join('LEFT JOIN', 'countries', 'countries.id = address.country_id') 
        ->join('LEFT JOIN', 'cities', 'cities.id = address.city_id') 
        ->all();
        if(!empty($mallsModel)) {
            echo Json::encode(array('status' => 'OK','data' => $mallsModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getMallById($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $id = $request->get('id');
        $mallsModel = (new \yii\db\Query())
        ->select(['malls.*', 'malls.id AS mall_id',
            'contacts.id AS contact_id', 'contacts.given_name', 'contacts.family_name', 'contacts.email as contact_email', 'contacts.phone_no AS contact_phone',
            'address.*', 'address.id AS address_id', 'countries.country', 'cities.city'])
        ->from('malls')
        ->join('LEFT JOIN', 'contacts', 'contacts.link_id = malls.id AND type_id=1')
        ->join('LEFT JOIN', 'address', 'address.id = malls.address_id')
        ->join('LEFT JOIN', 'countries', 'countries.id = address.country_id') 
        ->join('LEFT JOIN', 'cities', 'cities.id = address.city_id') 
        ->where('malls.id=:id', [':id' => $id])
        ->all();        
        if(!empty($mallsModel)) {
            echo Json::encode(array('status' => 'OK','data' => $mallsModel,'message' => 'Record found'));
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function editMalls($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $addressModel = Address::find()->where(['id' => $request->post('address_id')])->one();
        $addressModel->country_id = $request->post('country');
        $addressModel->city_id = $request->post('city');
        $addressModel->unit_no = $request->post('unit_no');
        $addressModel->street = $request->post('street');
        $addressModel->postal_code = $request->post('postal');
        $addressModel->updated_at = date('U');
        $addressModel->validate();
        $addressModel->save();
        
        $mallsModel = Malls::find()->where(['id' => $request->post('mall_id')])->one();
        $mallsModel->app_id = Yii::$app->params['appId'];
        $mallsModel->name = $request->post('name');
        $mallsModel->description = $request->post('description');
        $mallsModel->email = $request->post('email');
        $mallsModel->website = $request->post('website');
        $mallsModel->phone_no = $request->post('phone_no');
        $mallsModel->fax = $request->post('fax');
        $mallsModel->logo = $request->post('logo');
        $mallsModel->opening_hours = $request->post('opening_hours');
        $mallsModel->parking_hours = $request->post('parking_hours');
        $mallsModel->services = $request->post('services');
        $mallsModel->concierge = $request->post('concierge');
        $mallsModel->latitude = $request->post('latitude');
        $mallsModel->longitude = $request->post('longitude');
        $mallsModel->radius = $request->post('radius');
        $mallsModel->status = Malls::STATUS_ACTIVE;
        $mallsModel->updated_at = date('U');
        $mallsModel->validate();
        $error = $mallsModel->errors;
        
        if(empty($error)) {
            $mallsModel->save();
            $contactsModel = Contacts::find()->where(['id' => $request->post('contact_id')])->one();
            $contactsModel->given_name = $request->post('given_name');
            $contactsModel->family_name = $request->post('family_name');
            $contactsModel->email = $request->post('contact_email');
            $contactsModel->phone_no = $request->post('contact_phone');
            $contactsModel->updated_at = date('U');
            $contactsModel->save();            
            echo Json::encode(array('status' => 'OK', 'message' => 'Malls successfully added.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while adding Malls.'));
            die();
        }
    }
    
    public function getAllBeaconManufacturer() {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $beaconManufacturerModel = BeaconManufacturer::find()->orderBy(['name' => SORT_ASC])->all();
        if(!empty($beaconManufacturerModel)) {
            echo Json::encode(array('status' => 'OK','data' => $beaconManufacturerModel,'message' => 'Record found'));
            die();
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getAllBeaconType() {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $beaconTypeModel = BeaconType::find()->orderBy(['name' => SORT_ASC])->all();
        $beaconTypeModel = (new \yii\db\Query())
        ->select(['beacon_manufacturer.name AS manufacturer',
            'beacon_type.id AS id','beacon_type.name AS type'])
        ->from('beacon_type')
        ->join('LEFT JOIN', 'beacon_manufacturer', 'beacon_manufacturer.id = beacon_type.manufacturer_id')
        //->where('malls.id=:id', [':id' => $id])
        ->all();        
        if(!empty($beaconTypeModel)) {
            echo Json::encode(array('status' => 'OK','data' => $beaconTypeModel,'message' => 'Record found'));
            die();
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function getBeaconTypeByManufacturerId($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        $beaconManufacturerId = $request->post('beaconManufacturerId');
        
        $beaconTypeModel = BeaconType::find()->orderBy(['name' => SORT_ASC])->all();
        $beaconTypeModel = (new \yii\db\Query())
        ->select(['beacon_manufacturer.name AS manufacturer',
            'beacon_type.id AS id','beacon_type.name AS type'])
        ->from('beacon_type')
        ->join('LEFT JOIN', 'beacon_manufacturer', 'beacon_manufacturer.id = beacon_type.manufacturer_id')
        ->where('beacon_type.manufacturer_id=:manufacturer_id', [':manufacturer_id' => $beaconManufacturerId])
        ->all();        
        if(!empty($beaconTypeModel)) {
            echo Json::encode(array('status' => 'OK','data' => $beaconTypeModel,'message' => 'Record found'));
            die();
        } else { 
            echo Json::encode(array('status' => 'NOK','data' => array(),'message' => 'No record found'));
            die();
        }
    }
    
    public function addBeacon($request) {
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $beaconModel = new Address();
        $beaconModel->country_id = $request->post('country');
        $beaconModel->city_id = $request->post('city');
        $beaconModel->unit_no = $request->post('unit_no');
        $beaconModel->street = $request->post('street');
        $beaconModel->postal_code = $request->post('postal');
        $beaconModel->created_at = date('U');
        $beaconModel->updated_at = date('U');
        $beaconModel->save();
        $beaconModel->validate();
        
        $mallsModel = new Malls();
        $mallsModel->app_id = Yii::$app->params['appId'];
        $mallsModel->address_id = $addressModel->id;
        $mallsModel->name = $request->post('name');
        $mallsModel->description = $request->post('description');
        $mallsModel->email = $request->post('email');
        $mallsModel->website = $request->post('website');
        $mallsModel->phone_no = $request->post('phone_no');
        $mallsModel->fax = $request->post('fax');
        $mallsModel->logo = $request->post('logo');
        $mallsModel->opening_hours = $request->post('opening_hours');
        $mallsModel->parking_hours = $request->post('parking_hours');
        $mallsModel->services = $request->post('services');
        $mallsModel->concierge = $request->post('concierge');
        $mallsModel->latitude = $request->post('latitude');
        $mallsModel->longitude = $request->post('longitude');
        $mallsModel->radius = $request->post('radius');
        $mallsModel->status = Malls::STATUS_ACTIVE;
        $mallsModel->created_at = date('U');
        $mallsModel->updated_at = date('U');
        $mallsModel->validate();
        $error = $mallsModel->errors;
        if(empty($error)) {
            $mallsModel->save();
            $contactsModel = new Contacts();
            $contactsModel->type_id = ContactType::TYPE_MALL;
            $contactsModel->link_id = $mallsModel->id;
            $contactsModel->given_name = $request->post('given_name');
            $contactsModel->family_name = $request->post('family_name');
            $contactsModel->email = $request->post('contact_email');
            $contactsModel->phone_no = $request->post('contact_phone');
            $contactsModel->created_at = date('U');
            $contactsModel->updated_at = date('U');
            $contactsModel->save();            
            echo Json::encode(array('status' => 'OK', 'message' => 'Malls successfully added.'));
            die();
        } else {
            echo Json::encode(array('status' => 'NOK', 'message' => 'An error occured while adding Malls.'));
            die();
        }
    }
}
