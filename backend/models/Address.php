<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string $country_id
 * @property string $city_id
 * @property string $unit_no
 * @property string $street
 * @property int $postal_code
 * @property int $created_at
 * @property int $updated_at
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'city_id', 'street', 'postal_code', 'created_at', 'updated_at'], 'required'],
            [['postal_code', 'created_at', 'updated_at'], 'integer'],
            [['country_id', 'city_id', 'unit_no'], 'string', 'max' => 255],
            [['street'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'city_id' => 'City ID',
            'unit_no' => 'Unit No',
            'street' => 'Street',
            'postal_code' => 'Postal Code',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddressQuery(get_called_class());
    }
}
