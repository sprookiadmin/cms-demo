<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beacon_analytic".
 *
 * @property int $id
 * @property int $beacon_id
 * @property int $alert_count
 * @property int $access_count
 * @property int $created_at
 * @property int $updated_at
 */
class BeaconAnalytic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beacon_analytic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['beacon_id', 'alert_count', 'access_count', 'created_at', 'updated_at'], 'required'],
            [['beacon_id', 'alert_count', 'access_count', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'beacon_id' => 'Beacon ID',
            'alert_count' => 'Alert Count',
            'access_count' => 'Access Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return BeaconAnalyticQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BeaconAnalyticQuery(get_called_class());
    }
}
