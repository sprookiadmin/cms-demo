<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[BeaconAnalytic]].
 *
 * @see BeaconAnalytic
 */
class BeaconAnalyticQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BeaconAnalytic[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BeaconAnalytic|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
