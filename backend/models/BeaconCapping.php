<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beacon_capping".
 *
 * @property int $id
 * @property int $max_alerts
 * @property int $max_visits
 * @property int $created_at
 * @property int $updated_at
 */
class BeaconCapping extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beacon_capping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['max_alerts', 'max_visits', 'created_at', 'updated_at'], 'required'],
            [['max_alerts', 'max_visits', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'max_alerts' => 'Max Alerts',
            'max_visits' => 'Max Visits',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return BeaconCappingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BeaconCappingQuery(get_called_class());
    }
}
