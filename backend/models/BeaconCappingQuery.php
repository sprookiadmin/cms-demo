<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[BeaconCapping]].
 *
 * @see BeaconCapping
 */
class BeaconCappingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return BeaconCapping[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return BeaconCapping|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
