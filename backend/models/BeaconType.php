<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beacon_type".
 *
 * @property int $id
 * @property int $manufacturer_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 */
class BeaconType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beacon_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'name', 'created_at', 'updated_at'], 'required'],
            [['manufacturer_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_id' => 'Manufacturer ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return BeaconTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BeaconTypeQuery(get_called_class());
    }
}
