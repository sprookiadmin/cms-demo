<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "beacons".
 *
 * @property int $id
 * @property int $manufacturer_id
 * @property int $type_id
 * @property int $group_id
 * @property int $capping_id
 * @property string $name
 * @property string $alias
 * @property string $uuid
 * @property string $major
 * @property string $minor
 * @property string $range
 * @property string $technology
 * @property string $usage
 * @property string $sensor
 * @property string $thickness
 * @property string $length
 * @property string $width
 * @property string $weight
 * @property string $interval
 * @property string $battery_min_life
 * @property string $battery_max_life
 * @property string $battery_level
 * @property string $latitude
 * @property string $longitude
 * @property string $logo
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Beacons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beacons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'type_id', 'name', 'alias', 'uuid', 'major', 'minor', 'range', 'status', 'created_at', 'updated_at'], 'required'],
            [['manufacturer_id', 'type_id', 'group_id', 'capping_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias', 'uuid', 'major', 'minor', 'range', 'technology', 'usage', 'sensor', 'thickness', 'length', 'width', 'weight', 'interval', 'battery_min_life', 'battery_max_life', 'battery_level', 'latitude', 'longitude', 'logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_id' => 'Manufacturer ID',
            'type_id' => 'Type ID',
            'group_id' => 'Group ID',
            'capping_id' => 'Capping ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'uuid' => 'Uuid',
            'major' => 'Major',
            'minor' => 'Minor',
            'range' => 'Range',
            'technology' => 'Technology',
            'usage' => 'Usage',
            'sensor' => 'Sensor',
            'thickness' => 'Thickness',
            'length' => 'Length',
            'width' => 'Width',
            'weight' => 'Weight',
            'interval' => 'Interval',
            'battery_min_life' => 'Battery Min Life',
            'battery_max_life' => 'Battery Max Life',
            'battery_level' => 'Battery Level',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'logo' => 'Logo',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return BeaconsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BeaconsQuery(get_called_class());
    }
}
