<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "campaigns".
 *
 * @property int $id
 * @property int $merchant_id
 * @property int $currency_id
 * @property string $name
 * @property string $description
 * @property string $tnc
 * @property string $campaign_image
 * @property string $barcode_image
 * @property double $retail_value
 * @property double $campaign_value
 * @property int $product_volume
 * @property int $is_transactional
 * @property int $is_redeemable
 * @property int $type
 * @property string $promo_code
 * @property string $redemption_code
 * @property int $weight
 * @property int $start_time
 * @property int $end_time
 * @property int $created_at
 * @property int $updated_at
 */
class Campaigns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campaigns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['merchant_id', 'currency_id', 'name', 'retail_value', 'campaign_value', 'product_volume', 'is_transactional', 'is_redeemable', 'type', 'redemption_code', 'weight', 'start_time', 'end_time', 'created_at', 'updated_at'], 'required'],
            [['merchant_id', 'currency_id', 'product_volume', 'is_transactional', 'is_redeemable', 'type', 'weight', 'start_time', 'end_time', 'created_at', 'updated_at'], 'integer'],
            [['retail_value', 'campaign_value'], 'number'],
            [['name'], 'string', 'max' => 1000],
            [['description', 'tnc'], 'string', 'max' => 5000],
            [['campaign_image', 'barcode_image', 'promo_code', 'redemption_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merchant_id' => 'Merchant ID',
            'currency_id' => 'Currency ID',
            'name' => 'Name',
            'description' => 'Description',
            'tnc' => 'Tnc',
            'campaign_image' => 'Campaign Image',
            'barcode_image' => 'Barcode Image',
            'retail_value' => 'Retail Value',
            'campaign_value' => 'Campaign Value',
            'product_volume' => 'Product Volume',
            'is_transactional' => 'Is Transactional',
            'is_redeemable' => 'Is Redeemable',
            'type' => 'Type',
            'promo_code' => 'Promo Code',
            'redemption_code' => 'Redemption Code',
            'weight' => 'Weight',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CampaignsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CampaignsQuery(get_called_class());
    }
}
