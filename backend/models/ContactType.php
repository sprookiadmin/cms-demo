<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contact_type".
 *
 * @property int $id
 * @property string $type
 * @property int $created_at
 * @property int $updated_at
 */
class ContactType extends \yii\db\ActiveRecord
{
    const TYPE_MALL = 1;
    const TYPE_MERCHANT = 2;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ContactTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactTypeQuery(get_called_class());
    }
}
