<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[ContactType]].
 *
 * @see ContactType
 */
class ContactTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ContactType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ContactType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
