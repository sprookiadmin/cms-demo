<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property int $type_id
 * @property int $link_id
 * @property string $given_name
 * @property string $family_name
 * @property string $email
 * @property string $phone_no
 * @property int $created_at
 * @property int $updated_at
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'link_id', 'created_at', 'updated_at'], 'required'],
            [['type_id', 'link_id', 'created_at', 'updated_at'], 'integer'],
            [['given_name', 'family_name', 'email', 'phone_no'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'link_id' => 'Link ID',
            'given_name' => 'Given Name',
            'family_name' => 'Family Name',
            'email' => 'Email',
            'phone_no' => 'Phone No',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ContactsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactsQuery(get_called_class());
    }
}
