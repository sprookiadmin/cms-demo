<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "coupons".
 *
 * @property int $id
 * @property int $campaign_id
 * @property int $limit_type
 * @property int $limit
 * @property int $expiration_type
 * @property int $conditional_time
 * @property int $start_time
 * @property int $end_time
 * @property int $created_at
 * @property int $updated_at
 */
class Coupons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coupons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaign_id', 'limit_type', 'limit', 'expiration_type', 'conditional_time', 'start_time', 'end_time', 'created_at', 'updated_at'], 'required'],
            [['campaign_id', 'limit_type', 'limit', 'expiration_type', 'conditional_time', 'start_time', 'end_time', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Campaign ID',
            'limit_type' => 'Limit Type',
            'limit' => 'Limit',
            'expiration_type' => 'Expiration Type',
            'conditional_time' => 'Conditional Time',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CouponsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CouponsQuery(get_called_class());
    }
}
