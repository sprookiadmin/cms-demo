<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "malls".
 *
 * @property int $id
 * @property int $app_id
 * @property int $address_id
 * @property string $name
 * @property string $description
 * @property string $email
 * @property string $website
 * @property string $opening_hours
 * @property string $parking_hours
 * @property string $concierge
 * @property string $services
 * @property string $phone_no
 * @property string $fax
 * @property int $status
 * @property string $latitude
 * @property string $longitude
 * @property int $radius
 * @property string $logo
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Address $address
 */
class Malls extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2; 
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'malls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['app_id', 'address_id', 'name', 'description', 'status', 'latitude', 'longitude', 'radius', 'created_at', 'updated_at'], 'required'],
            [['app_id', 'address_id', 'status', 'radius', 'created_at', 'updated_at'], 'integer'],
            [['name', 'email', 'website', 'opening_hours', 'parking_hours', 'concierge', 'latitude', 'longitude'], 'string', 'max' => 255],
            [['description', 'services'], 'string', 'max' => 1000],
            [['phone_no', 'fax'], 'string', 'max' => 30],
            [['logo'], 'string', 'max' => 200],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'app_id' => 'App ID',
            'address_id' => 'Address ID',
            'name' => 'Name',
            'description' => 'Description',
            'email' => 'Email',
            'website' => 'Website',
            'opening_hours' => 'Opening Hours',
            'parking_hours' => 'Parking Hours',
            'concierge' => 'Concierge',
            'services' => 'Services',
            'phone_no' => 'Phone No',
            'fax' => 'Fax',
            'status' => 'Status',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'radius' => 'Radius',
            'logo' => 'Logo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * {@inheritdoc}
     * @return MallsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MallsQuery(get_called_class());
    }
}
