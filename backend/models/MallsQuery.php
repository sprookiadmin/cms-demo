<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Malls]].
 *
 * @see Malls
 */
class MallsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Malls[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Malls|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
