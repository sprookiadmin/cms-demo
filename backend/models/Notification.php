<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $user_id
 * @property string $beacon_id
 * @property int $beacon_group_id
 * @property string $notification
 * @property string $proximity
 * @property int $major
 * @property int $minor
 * @property int $created_at
 * @property int $updated_at
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'beacon_id', 'beacon_group_id', 'notification', 'proximity', 'major', 'minor', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'beacon_group_id', 'major', 'minor', 'created_at', 'updated_at'], 'integer'],
            [['beacon_id', 'notification', 'proximity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'beacon_id' => 'Beacon ID',
            'beacon_group_id' => 'Beacon Group ID',
            'notification' => 'Notification',
            'proximity' => 'Proximity',
            'major' => 'Major',
            'minor' => 'Minor',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return NotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NotificationQuery(get_called_class());
    }
}
