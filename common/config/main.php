<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        ],
        'urlManager' => [
            'enablePrettyUrl'       => true,
            'showScriptName'        => false,
            'enableStrictParsing'   => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'admin/notification-edit/<id:\d+>' => 'admin/notification-edit',
                'admin/malls-edit/<id:\d+>' => 'admin/malls-edit',
                'admin/malls-view/<id:\d+>' => 'admin/malls-view',
                'admin/beacon-group-edit/<id:\d+>' => 'admin/beacon-group-edit',
                'defaultRoute' => '/site/index',
            ],

        ],
        'assetManager' => [
            'linkAssets' => true,
            
        ],
    ],
    'modules' => [
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
        ],
    ],
];
