<?php
return [
    'adminEmail' => 'felixander.loetama@invigorgroup.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'apiToken' => 'ASD34324kd',
    'apiUrl' => 'http://invigor-api.dbs.com/api',
    'appId' => '1',
];
