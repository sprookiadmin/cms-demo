<?php

use yii\db\Migration;

/**
 * Class m190607_034453_notification
 */
class m190607_034453_notification extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'beacon_id' => $this->string()->notNull(),
            'beacon_group_id' => $this->integer()->notNull(),
            'notification' => $this->string()->notNull(),
            'proximity' => $this->string()->notNull(),
            'major' => $this->integer()->notNull(),
            'minor' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%notification}}');
    }
}
