<?php

use yii\db\Migration;

/**
 * Class m190614_072053_campaigns
 */
class m190614_072053_campaigns extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%campaigns}}', [
            'id' => $this->primaryKey(),
            'merchant_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'name' => $this->string(1000)->notNull(),
            'description' => $this->string(5000),
            'tnc' => $this->string(5000),
            'campaign_image' => $this->string(),
            'barcode_image' => $this->string(),
            'retail_value' => $this->float()->notNull(),
            'campaign_value' => $this->float()->notNull(),
            'product_volume' => $this->integer()->notNull(),
            'is_transactional' => $this->tinyInteger(1)->notNull(),
            'is_redeemable' => $this->tinyInteger(1)->notNull(),
            'type' => $this->integer()->notNull(),
            'promo_code' => $this->string(),
            'redemption_code' => $this->string()->notNull(),
            'weight' => $this->integer()->notNull(),
            'start_time' => $this->integer()->notNull(),
            'end_time' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%campaigns}}');
    }
}
