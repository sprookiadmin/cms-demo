<?php

use yii\db\Migration;

/**
 * Class m190614_082739_coupons
 */
class m190614_082739_coupons extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%coupons}}', [
            'id' => $this->primaryKey(),
            'campaign_id' => $this->integer()->notNull(),
            'limit_type' => $this->integer()->notNull(),
            'limit' => $this->integer()->notNull(),
            'expiration_type' => $this->integer()->notNull(),
            'conditional_time' => $this->integer()->notNull(),
            'start_time' => $this->integer()->notNull(),
            'end_time' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%coupons}}');
    }
}
