<?php

use yii\db\Migration;

/**
 * Class m190701_072829_address
 */
class m190701_072829_address extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%address}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->string()->notNull(),
            'city_id' => $this->string()->notNull(),
            'unit_no' => $this->string(),
            'street' => $this->string(1000)->notNull(),
            'postal_code' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%address}}');
    }
}
