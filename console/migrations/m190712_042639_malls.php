<?php

use yii\db\Migration;

/**
 * Class m190712_042639_malls
 */
class m190712_042639_malls extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%malls}}', [
            'id' => $this->primaryKey(),
            'app_id' => $this->integer()->notNull(),
            'address_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(1000)->notNull(),
            'email' => $this->string(),
            'website' => $this->string(),
            'opening_hours' => $this->string(),
            'parking_hours' => $this->string(),
            'concierge' => $this->string(),
            'services' => $this->string(1000),
            'phone_no' => $this->string(30),
            'fax' => $this->string(30),
            'status' => $this->integer(4)->notNull(),
            'latitude' => $this->string()->notNull(),
            'longitude' => $this->string()->notNull(),
            'radius' => $this->integer()->notNull(),
            'logo' => $this->string(200),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex(
            'idx-malls-address_id',
            'malls',
            'address_id'
        );
        $this->addForeignKey(
            'fk-malls-address_id',
            'malls',
            'address_id',
            'address',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%malls}}');
        $this->dropIndex(
            'idx-malls-address_id',
            'malls'
        );
        $this->dropForeignKey(
            'fk-malls-address_id',
            'malls'
        );
    }
}
