<?php

use yii\db\Migration;

/**
 * Class m190718_070959_merchants
 */
class m190718_070959_merchants extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%merchants}}', [
            'id' => $this->primaryKey(),
            'app_id' => $this->integer()->notNull(),
            'address_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(1000)->notNull(),
            'email' => $this->string(),
            'website' => $this->string(),
            'notes' => $this->string(1000),
            'status' => $this->integer(4)->notNull(),
            'latitude' => $this->string()->notNull(),
            'longitude' => $this->string()->notNull(),
            'logo' => $this->string(200),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex(
            'idx-merchants-address_id',
            'merchants',
            'address_id'
        );
        $this->addForeignKey(
            'fk-merchants-address_id',
            'merchants',
            'address_id',
            'address',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%merchants}}');
        $this->dropIndex(
            'idx-merchants-address_id',
            'merchants'
        );
        $this->dropForeignKey(
            'fk-merchants-address_id',
            'merchants'
        );
    }
}
