<?php

use yii\db\Migration;

/**
 * Class m190718_072342_contacts
 */
class m190718_072342_contacts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contacts}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'link_id' => $this->integer()->notNull(),
            'given_name' => $this->string(),
            'family_name' => $this->string(),
            'email' => $this->string(),
            'phone_no' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%contacts}}');
    }
}
