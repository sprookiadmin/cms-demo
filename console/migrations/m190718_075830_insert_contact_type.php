<?php

use yii\db\Migration;

/**
 * Class m190718_075830_insert_contact_type
 */
class m190718_075830_insert_contact_type extends Migration
{
    public function up()
    {
        $this->insert('contact_type',array(
            'type'=>'mall',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('contact_type',array(
            'type'=>'merchant',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
    }
    
    public function down()
    {
        
    }
    
}
