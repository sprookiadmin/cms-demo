<?php

use yii\db\Migration;

/**
 * Class m190719_061252_beacons
 */
class m190719_061252_beacons extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%beacons}}', [
            'id' => $this->primaryKey(),
            'manufacturer_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'group_id' => $this->integer(),
            'capping_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'uuid' => $this->string()->notNull(),
            'major' => $this->string()->notNull(),
            'minor' => $this->string()->notNull(),
            'range' => $this->string()->notNull(),
            'technology' => $this->string(),
            'usage' => $this->string(),
            'sensor' => $this->string(),
            'thickness' => $this->string(),
            'length' => $this->string(),
            'width' => $this->string(),
            'weight' => $this->string(),
            'interval' => $this->string(),
            'battery_min_life' => $this->string(),
            'battery_max_life' => $this->string(),
            'battery_level' => $this->string(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'logo' => $this->string(),
            'status' => $this->integer(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%beacons}}');
    }
}
