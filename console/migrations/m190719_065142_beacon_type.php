<?php

use yii\db\Migration;

/**
 * Class m190719_065142_beacon_type
 */
class m190719_065142_beacon_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%beacon_type}}', [
            'id' => $this->primaryKey(),
            'manufacturer_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%beacon_type}}');
    }
}
