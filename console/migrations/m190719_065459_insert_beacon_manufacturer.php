<?php

use yii\db\Migration;

/**
 * Class m190719_065459_insert_beacon_manufacturer
 */
class m190719_065459_insert_beacon_manufacturer extends Migration
{
    public function up()
    {
        $this->insert('beacon_manufacturer',array(
            'name'=>'Estimote',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_manufacturer',array(
            'name'=>'Kontak.io',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_manufacturer',array(
            'name'=>'Overdrive',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
    }
    
    public function down()
    {
        
    }
}
