<?php

use yii\db\Migration;

/**
 * Class m190719_065512_insert_beacon_type
 */
class m190719_065512_insert_beacon_type extends Migration
{
    public function up()
    {
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'Sticker Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'Proximity Tag',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'Proximity Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'Location Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'Location UWB Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'Video Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'1',
            'name'=>'LTE Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Asset Tag',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Bracelet Tag',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Universal Tag',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Card Tag',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Smart Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Tough Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'2',
            'name'=>'Heavy Duty Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'3',
            'name'=>'iBeacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
        $this->insert('beacon_type',array(
            'manufacturer_id'=>'3',
            'name'=>'Bluetooth Beacon',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ));
    }
    
    public function down()
    {
        
    }
}
