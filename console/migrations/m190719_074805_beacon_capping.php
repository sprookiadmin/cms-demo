<?php

use yii\db\Migration;

/**
 * Class m190719_074805_beacon_capping
 */
class m190719_074805_beacon_capping extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%beacon_capping}}', [
            'id' => $this->primaryKey(),
            'max_alerts' => $this->integer()->notNull(),
            'max_visits' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%beacon_capping}}');
    }
}
