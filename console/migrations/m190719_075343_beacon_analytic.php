<?php

use yii\db\Migration;

/**
 * Class m190719_075343_beacon_analytic
 */
class m190719_075343_beacon_analytic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%beacon_analytic}}', [
            'id' => $this->primaryKey(),
            'beacon_id' => $this->integer()->notNull(),
            'alert_count' => $this->integer()->notNull(),
            'access_count' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%beacon_analytic}}');
    }
}
