<?php

use yii2mod\rbac\migrations\Migration;

class m190614_060724_create_role_admin extends Migration
{
    public function safeUp()
    {
        $this->createRole('admin', 'admin has all available permissions.');
    }

    public function safeDown()
    {
        $this->removeRole('admin');
    }
}