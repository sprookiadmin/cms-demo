<?php
return [
    'adminEmail' => 'felixander.loetama@invigorgroup.com',
    'secretKey' => 'AXAg2343l@xM',
    'beacon_sensor' => 
    [
        'accelerometer',
        'temperature',
        'ambient light',
        'magnetometer'
    ],
    'beacon_technology' => 
    [        
        'Bluetooth 4.2', 
        'Bluetooth 5.0',
	'UWB',
        'WiFi',	
        'LTE-M',
        'NB-IoT',
        'GPS', 
        'GALILEO',
        'GLONASS'
    ],
    'beacon_usage' => 
    [
        'asset tracking',
        'vehicle tracking',
        'proximity gateway',
        'digital signage',
        'indoor location'
    ],    
];
