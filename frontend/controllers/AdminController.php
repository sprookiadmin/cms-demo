<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use linslin\yii2\curl;
use yii\helpers\Json;

/**
 * Admin controller
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function __construct($id, $module, $config = [])
    {
        Yii::$app->assetManager->bundles = [
            'yii\web\JqueryAsset' => [
                'js'=>[]
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'js'=>[]
            ],
            'yii\bootstrap\BootstrapAsset' => [
                'css' => [],
            ],
        ];
        parent::__construct($id, $module, $config);
    }
    
    public function actionIndex()
    {
        $this->layout = 'admin';
        return $this->render('index');
    }

    public function actionSecurity()
    {
        $request = Yii::$app->request;
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $response = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getApiKey&token='.$apiToken, true);
        $responseData = Json::decode($response);
        $apiKey = '';
        if(isset($responseData['api_key'])) {
            $apiKey = $responseData['api_key'];
        }        
        return $this->render('security', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'apiKey' => $apiKey]);
    }
    
    public function actionGatewayList() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $api = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getApiKey&token='.$apiToken, true);
        $apiData = Json::decode($api);
        $apiKey = '';
        if(isset($apiData['api_key'])) {
            $apiKey = $apiData['api_key'];
        } 
        $beaconApiUrl = 'https://api.kontakt.io/device?access=SUPERVISOR&maxResult=500';
        $curl = new curl\Curl();
        $curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
        $curl->setOption(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOption(CURLOPT_RETURNTRANSFER, true);
        $curl->setOption(CURLOPT_HTTPHEADER, array('Api-Key: '.$apiKey, 'Accept: application/vnd.com.kontakt+json;version=10'));
        $beacon = $curl->get($beaconApiUrl);
        $beaconData = Json::decode($beacon);
        return $this->render('gateway-list', ['beaconData' => $beaconData['devices']]);
    }
    
    public function actionNotificationAdd() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $beacon = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getAllBeacon&token='.$apiToken, true);
        $beaconData = Json::decode($beacon);
        $beaconGroup = $curl->setGetParams([
            'action' => 'getAllBeaconGroup',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $beaconGroup = Json::decode($beaconGroup);
        return $this->render('notification-add', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'beaconData' => $beaconData['devices'], 'beaconGroupData' => $beaconGroup['data']]);
    }    
    
    public function actionNotificationList() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $notification = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getAllNotification&token='.$apiToken, true);
        $notification = Json::decode($notification);
        $beaconGroup = $curl->setGetParams([
            'action' => 'getAllBeaconGroup',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $beaconGroup = Json::decode($beaconGroup);
        foreach($beaconGroup['data'] as $no => $bg) {
            $beaconGroupData[$bg['id']] = $bg['name'];
        }
        return $this->render('notification-list', ['notificationData' => $notification['data'], 'beaconGroupData' => $beaconGroupData]);
    }    
    
    public function actionNotificationEdit($id) 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $beacon = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getAllBeacon&token='.$apiToken, true);
        $beaconData = Json::decode($beacon);
        $notification = $curl->setRawPostData(['notificationId' => $id])->post($apiUrl.'?action=getNotificationById&token='.$apiToken, true);
        $notification = Json::decode($notification);
        $beaconGroup = $curl->setGetParams([
            'action' => 'getAllBeaconGroup',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $beaconGroup = Json::decode($beaconGroup);
        foreach($beaconGroup['data'] as $no => $bg) {
            $beaconGroupData[$bg['id']] = $bg['name'];
        }
        return $this->render('notification-edit', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'beaconData' => $beaconData['devices'], 'notificationData' => $notification['data'], 'beaconGroupData' => $beaconGroupData]);
    } 
    
    public function actionBeaconList() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $beacon = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getAllBeacon&token='.$apiToken, true);
        $beaconData = Json::decode($beacon);
        return $this->render('beacon-list', ['beaconData' => $beaconData['devices']]);
    }
    
    public function actionBeaconAdd() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $beaconGroup = $curl->setGetParams([
            'action' => 'getAllBeaconGroup',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $beaconGroup = Json::decode($beaconGroup);
        $beaconType = $curl->setGetParams([
            'action' => 'getAllBeaconType',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $beaconType = Json::decode($beaconType);
        $beaconManufacturer = $curl->setGetParams([
            'action' => 'getAllBeaconManufacturer',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $beaconManufacturer = Json::decode($beaconManufacturer);
        return $this->render('beacon-add', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'sensor' => Yii::$app->params['beacon_sensor'], 'technology' => Yii::$app->params['beacon_technology'], 'usage' => Yii::$app->params['beacon_usage'], 'beacon_manufacturer' => $beaconManufacturer['data'], 'beacon_group' => $beaconGroup['data']]);
    }
    
    public function actionBeaconGroupAdd() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        return $this->render('beacon-group-add', ['apiUrl' => $apiUrl, 'apiToken' => $apiToken]);
    }    
    
    public function actionBeaconGroupList() 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $beaconGroup = $curl->setGetParams([
            'action' => 'getAllBeaconGroup',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $beaconGroup = Json::decode($beaconGroup);
        return $this->render('beacon-group-list', ['beaconGroupData' => $beaconGroup['data']]);
    }  
    
    public function actionBeaconGroupEdit($id) 
    {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $beaconGroup = $curl->setRawPostData(['beaconGroupId' => $id])->post($apiUrl.'?action=getBeaconGroupById&token='.$apiToken, true);
        $beaconGroup = Json::decode($beaconGroup);
        return $this->render('beacon-group-edit', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'beaconGroupData' => $beaconGroup['data']]);
    } 
    
    public function actionMallsAdd() {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $countries = $curl->setGetParams([
            'action' => 'getCountries',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $countries = Json::decode($countries, true);
        return $this->render('malls-add', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'countries' => $countries['data']]);
    }
    
    public function actionMallsList() {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $malls = $curl->setGetParams([
            'action' => 'getMalls',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $malls = Json::decode($malls);
        return $this->render('malls-list', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'malls' => $malls['data']]);
    }
    
    public function actionMallsEdit($id) {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $malls = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getMallById&id='.$id.'&token='.$apiToken, true);
        $malls = Json::decode($malls);
        $countries = $curl->setGetParams([
            'action' => 'getCountries',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $countries = Json::decode($countries, true);
        $cities = $curl->setGetParams([
            'action' => 'getCities',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $cities = Json::decode($cities, true);
        return $this->render('malls-edit', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'malls' => $malls['data'][0], 'countries' => $countries['data'],'cities' => $cities['data']]);
    }
    
    public function actionMallsView($id) {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $malls = $curl->setRawPostData(['userId' => $user->id])->post($apiUrl.'?action=getMallById&id='.$id.'&token='.$apiToken, true);
        $malls = Json::decode($malls);
        $countries = $curl->setGetParams([
            'action' => 'getCountries',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $countries = Json::decode($countries, true);
        $cities = $curl->setGetParams([
            'action' => 'getCities',
            'token' => $apiToken
         ])->get($apiUrl, true);
        $cities = Json::decode($cities, true);
        return $this->render('malls-view', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'malls' => $malls['data'][0], 'countries' => $countries['data'],'cities' => $cities['data']]);
    }
    
    public function actionBeaconManufacturerList() {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $manufacturers = $curl->setGetParams([
            'action' => 'getAllBeaconManufacturer',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $manufacturers = Json::decode($manufacturers);
        return $this->render('beacon-manufacturer-list', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'manufacturers' => $manufacturers['data']]);
    }
    
    public function actionBeaconTypeList() {
        $this->layout = 'admin';
        $user = Yii::$app->user->identity;
        $apiUrl = Yii::$app->params['apiUrl'];
        $apiToken = Yii::$app->params['apiToken'];
        $curl = new curl\Curl();
        $types = $curl->setGetParams([
            'action' => 'getAllBeaconType',
            'token' => $apiToken
        ])->get($apiUrl, true);
        $types = Json::decode($types);
        return $this->render('beacon-type-list', ['user' => $user, 'apiUrl' => $apiUrl, 'apiToken' => $apiToken, 'types' => $types['data']]);
    }
}
