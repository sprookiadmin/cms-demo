<?php
/* @var $this yii\web\View */

//use Yii;
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Add Beacon';

$script = <<< JS
    jQuery(document).ready(function() {
        jQuery(".chosen-select").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
JS;
$this->registerJs($script, View::POS_HEAD);
$beaconUrl = $apiUrl . '?action=addBeacon&token=' . $apiToken;
$redirectUrl = Yii::$app->getUrlManager()->getBaseUrl() . '/admin/beacon-list';
$script = <<< JS
    function addBeacon() {
        upload();
        setTimeout(function(){
            $.ajax({
                url: '$beaconUrl',
                dataType: 'json',
                type: 'POST',
                data: {
                    name : $('#name').val(),
                    alias : $('#alias').val(),
                    logo :  $('#logo').val(),
                    uuid :  $('#uuid').val(),
                    major :  $('#major').val(),
                    minor :  $('#minor').val(),
                    range :  $('#range').val(),
                    manufacturer : $('#manufacturer').val(),
                    type : $('#type').val(),
                    group : $('#group').val(),
                    max_alerts : $('#max_alerts').val(),
                    max_visits : $('#max_visits').val(),
                    technology : $('#technology').val(),
                    usage : $('#usage').val(),
                    sensor : $('#sensor').val(),
                    thickness : $('#thickness').val(),
                    length : $('#length').val(),
                    width : $('#width').val(),
                    weight : $('#weight').val(),
                    interval : $('#interval').val(),
                    battery_min_life : $('#battery_min_life').val(),
                    battery_max_life : $('#battery_max_life').val(),
                },
                success: function(data){
                    if(data.status == 'NOK') {
                        $('#error').html(data.message).show();
                        $('#success').hide();    
                    } else {
                        $('#success').html(data.message).show();
                        $('#error').hide();
                        window.location.href='$redirectUrl';
                    }
                }
            });
        }, 2000);
    }
JS;
$this->registerJs($script, View::POS_HEAD);
$this->registerCssFile("@web/js/chosen/chosen.min.css");
$this->registerCssFile("@web/css/jquery-ui.min.css");
?>
<div id="success" class="alert alert-success" role="alert" style="display:none">
</div>
<div id="error" class="alert alert-danger" role="alert" style="display:none">
</div>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Add Beacon</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Beacon Details</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="name" class=" form-control-label">*Name</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="name" name="name" value="" placeholder="Enter Name" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="alias" class=" form-control-label">Alias</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="alias" name="alias" value="" placeholder="Enter Alias" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="uuid" class=" form-control-label">*UUID</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="uuid" name="uuid" value="" placeholder="Enter UUID" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="major" class=" form-control-label">*Major</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="major" name="major" value="" placeholder="Enter Major" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="minor" class=" form-control-label">*Minor</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="minor" name="minor" value="" placeholder="Enter Minor" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="range" class=" form-control-label">*Range</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="range" name="range" value="" placeholder="Enter Range" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="logo" class=" form-control-label">Logo</label></div>
                            <div class="col-12 col-md-9">
                                <form method="post" id="fileinfo" name="fileinfo" action="" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" multiple class="custom-file-input" name="filesToUpload[]" id="filesToUpload" onChange="processUpload();">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose files</label>
                                        </div>
                                        <?= Html::img('@web/img/loading.gif', ['alt'=>'Logo', 'id'=>'loadingmessage', 'style' => 'display:none']); ?>
                                    </div>
                                </form>
                                <br>
                                <div id="fileList">No Files Selected</div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label"></label></div>
                            <div class="col-12 col-md-9">
                                Image size should be at least 512 x 512 pixels<br>
                                Supported formats PNG, JPEG, GIF
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Beacon Setting</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="manufacturer" class=" form-control-label">Manufacturer</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select a manufacturer" id="manufacturer" name="manufacturer" onchange="loadType();" class="chosen-select" tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($beacon_manufacturer as $no => $bm) {
                                            echo ' <option value="'.$bm['id'].'">'.$bm['name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group" id="typeField" style="display:none;">
                            <div class="col col-md-3"><label for="type" class=" form-control-label">Type</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select a type" id="type" name="type" class="chosen-select" tabindex="1">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="group" class=" form-control-label">Group</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select a group" id="group" name="group" class="chosen-select" tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($beacon_group as $no => $bg) {
                                            echo ' <option value="'.$bg['id'].'">'.$bg['name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="max_alerts" class=" form-control-label">Max Alerts</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="max_alerts" name="max_alerts" value="" placeholder="Enter Max Alerts" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="max_visits" class=" form-control-label">Max Visits</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="max_visits" name="max_visits" value="" placeholder="Enter Max Visits" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Beacon Information</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="technology" class=" form-control-label">Technology</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select technology" id="technology" name="technology" class="chosen-select" multiple tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($sensor as $s) {
                                            echo ' <option value="'.$s.'">'.$s.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="usage" class=" form-control-label">Usage</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select usage" id="usage" name="usage" class="chosen-select" multiple tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($usage as $u) {
                                            echo ' <option value="'.$u.'">'.$u.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="sensor" class=" form-control-label">Sensor</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select a sensor" id="sensor" name="sensor" class="chosen-select" multiple tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($sensor as $s) {
                                            echo ' <option value="'.$s.'">'.$s.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="thickness" class=" form-control-label">Thickness</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="thickness" name="thickness" value="" placeholder="Enter Thickness in mm" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="length" class=" form-control-label">Length</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="length" name="length" value="" placeholder="Enter Length in mm" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="width" class=" form-control-label">Width</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="width" name="width" value="" placeholder="Enter Width in mm" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="weight" class=" form-control-label">Weight</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="weight" name="weight" value="" placeholder="Enter Weight in gram" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="interval" class=" form-control-label">Interval</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="interval" name="interval" value="" placeholder="Enter Interval in ms" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="battery_min_life" class=" form-control-label">Battery Min Life</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="battery_min_life" name="battery_min_life" value="" placeholder="Enter Battery Min Life in year" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="battery_max_life" class=" form-control-label">Battery Max Life</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="battery_max_life" name="battery_max_life" value="" placeholder="Enter Battery Max Life in year" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" onclick="addBeacon();" class="btn btn-primary btn-sm pull-right">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<input type="hidden" id="logo" name="logo" value="">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    function processUpload() {
        var input = document.getElementById("filesToUpload");
        var ul = document.getElementById("fileList");
        while (ul.hasChildNodes()) {
            ul.removeChild(ul.firstChild);
        }
        for (var i = 0; i < input.files.length; i++) {
            var li = document.createElement("li");
            if(isFileImage(input.files[i].type) === false) {
                li.innerHTML = "<span style='color:red'>"+input.files[i].name+" not supported format.</span>";
                ul.appendChild(li);
                break;
            }
            li.innerHTML = input.files[i].name;
            ul.appendChild(li);
        }
        if(!ul.hasChildNodes()) {
            var li = document.createElement("li");
            li.innerHTML = 'No Files Selected';
            ul.appendChild(li);
        }
    }
    
    function upload() {
        $('#loadingmessage').show();
        var url = '<?=$apiUrl.'?action=uploadImages&token='.$apiToken?>';
        var fd = new FormData(document.getElementById("fileinfo"));
        var input = document.getElementById("filesToUpload");
        if(input.files.length < 1) {
            $('#fileList').html("<span style='color:red'>Please select an image.</span>");
            $('#loadingmessage').hide();
        } else {
            $.ajax(url, {
                type: 'POST',  
                data : fd,
                processData: false,  
                contentType: false,  
                enctype: 'multipart/form-data',
                success: function (r) {
                    $('#loadingmessage').hide();
                    $('#logo').val(r.data.file.join());
                    var ul = document.getElementById("fileList");
                    while (ul.hasChildNodes()) {
                        ul.removeChild(ul.firstChild);
                    }
                    for (var i = 0; i < input.files.length; i++) {
                        var li = document.createElement("li");
                        li.innerHTML = "<span style='color:green'>"+input.files[i].name+" uploaded.</span>";
                        ul.appendChild(li);
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    alert('error');
                }
            });
        }
    }
    
    function isFileImage(type) {
        const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
        return acceptedImageTypes.includes(type);
    }
    
    function isAllowedImageSize(height, width) {
        if(height < 512 || width < 512) {
            return false;
        } 
        return true;
    }
    
    function loadType() {
        var url = '<?=$apiUrl.'?action=getBeaconTypeByManufacturerId&token='.$apiToken?>';
        $.ajax(url, {
            type: 'POST',  
            data : {
                beaconManufacturerId : $("#manufacturer").val()
            },
            success: function (r) {
                $('#typeField').show();
                $('#type').empty();
                for(var i=0; i < r.data.length; i++) {
                    $('#type').append('<option value="'+r.data[i].id+'">'+r.data[i].type+'</option>');
                }
                $('#type').trigger("chosen:updated");
            },
            error: function (jqXhr, textStatus, errorMessage) {
                alert('error');
            }
        });
    }
</script>
<?php
$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);
//$this->registerJsFile("@web/js/main.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/chosen/chosen.jquery.min.js", ['position' => View::POS_HEAD]);

?>