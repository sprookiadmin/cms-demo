<?php
/* @var $this yii\web\View */

//use Yii;
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Edit BeaconGroup';
$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);

$beaconGroupUrl = $apiUrl . '?action=updateBeaconGroup&token=' . $apiToken;
$redirectUrl = Yii::$app->getUrlManager()->getBaseUrl() . '/admin/beacon-group-list';
$script = <<< JS
    function updateBeaconGroup() {
        $.ajax({
            url: '$beaconGroupUrl',
            dataType: 'json',
            type: 'POST',
            data: {
                beaconGroupId : $('#beaconGroupId').val(),
                beaconGroupName : $('#beaconGroupName').val(),
            },
            success: function(data){
                if(data.status == 'NOK') {
                    $('#error').html(data.message).show();
                    $('#success').hide();    
                } else {
                    $('#success').html(data.message).show();
                    $('#error').hide();
                    window.location.href='$redirectUrl';
                }
            }
        });
    }
JS;
$this->registerJs($script, View::POS_END);
?>
<div id="success" class="alert alert-success" role="alert" style="display:none">
</div>
<div id="error" class="alert alert-danger" role="alert" style="display:none">
</div>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Edit Beacon Group</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Edit Beacon Group</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="beaconGroupName" class=" form-control-label">Beacon Group</label></div>
                            <div class="col-12 col-md-9">
                                <input type="hidden" id="beaconGroupId" name="beaconGroupId" value="<?= $beaconGroupData['id']; ?>">
                                <input type="text" id="beaconGroupName" name="beaconGroupName" value="<?= $beaconGroupData['name']; ?>" placeholder="Enter Beacon Group" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" onclick="updateBeaconGroup();" class="btn btn-success btn-sm">Submit</button>
                    </div>
                </div>
            </div>            
        </div>
    </div><!-- .animated -->
</div><!-- .content -->