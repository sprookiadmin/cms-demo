<?php
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Beacon Manufacturer List';

$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/main.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net/js/jquery.dataTables.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-bs4/js/dataTables.bootstrap4.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/dataTables.buttons.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/buttons.html5.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/buttons.print.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/buttons.colVis.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/init-scripts/data-table/datatables-init.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/jszip/dist/jszip.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/pdfmake/build/pdfmake.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/pdfmake/build/vfs_fonts.js", ['position' => View::POS_END]);

$this->registerCssFile("@web/js/datatables.net-bs4/css/dataTables.bootstrap4.min.css");
$this->registerCssFile("@web/js/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css");
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Beacon Type List</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Beacon Type List</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Manufacturer</th>
                                    <th>Beacon Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($types as $no => $type) {
                                    echo ' <tr>';
                                    echo '<td>'.$type['type'].'</td>';
                                    echo '<td>'.$type['manufacturer'].'</td>';
                                    $option = 
                                    '<td><a href="'.Yii::$app->getUrlManager()->getBaseUrl() . '/admin/beacon-type-edit/'.$type['id'].'">
                                        <span class="fa fa-edit"></span>
                                    </a>&nbsp;|&nbsp;
                                    <a href="'.Yii::$app->getUrlManager()->getBaseUrl() . '/admin/beacon-type-remove/'.$type['id'].'">
                                        <span class="fa fa-trash"></span>
                                    </a></td>';
                                    echo $option;
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->