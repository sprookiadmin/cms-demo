<?php
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Gateway List';

$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/main.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net/js/jquery.dataTables.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-bs4/js/dataTables.bootstrap4.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/dataTables.buttons.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/buttons.html5.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/buttons.print.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/datatables.net-buttons/js/buttons.colVis.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/init-scripts/data-table/datatables-init.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/jszip/dist/jszip.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/pdfmake/build/pdfmake.min.js", ['position' => View::POS_END]);
$this->registerJsFile("@web/js/pdfmake/build/vfs_fonts.js", ['position' => View::POS_END]);

$this->registerCssFile("@web/js/datatables.net-bs4/css/dataTables.bootstrap4.min.css");
$this->registerCssFile("@web/js/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css");
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Gateway List</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Gateway List</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Alias</th>
                                    <th>Firmware</th>
                                    <th>Tags</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Order ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($beaconData as $no => $beacon) {
                                    if(isset($beacon['deviceType']) && ($beacon['deviceType'] == 'CLOUD_BEACON' || $beacon['deviceType'] == 'EXTERNAL')) {
                                        $firmware = isset($beacon['firmware']) ? $beacon['firmware'] : '';
                                        $product = isset($beacon['product']) ? $beacon['product'] : '';
                                        $orderId = isset($beacon['orderId']) ? $beacon['orderId'] : '';
                                        echo ' <tr>';
                                        echo '<td>'.$beacon['uniqueId'].'</td>';
                                        echo '<td>'.$beacon['alias'].'</td>';
                                        echo '<td>'.$firmware.'</td>';
                                        echo '<td>'.implode(',',$beacon['tags']).'</td>';
                                        echo '<td>'.$product.'</td>';
                                        echo '<td>'.$beacon['lat'].':'.$beacon['lng'].'</td>';
                                        echo '<td>'.$orderId.'</td>';
                                        echo '</tr>';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->