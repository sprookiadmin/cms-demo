<?php
/* @var $this yii\web\View */

//use Yii;
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Edit Malls';

$script = <<< JS
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
JS;
$this->registerJs($script, View::POS_HEAD);
$mallsUrl = $apiUrl . '?action=editMalls&token=' . $apiToken;
$redirectUrl = Yii::$app->getUrlManager()->getBaseUrl() . '/admin/malls-list';
$script = <<< JS
    function editMalls() {
        upload();
        setTimeout(function(){
            $.ajax({
                url: '$mallsUrl',
                dataType: 'json',
                type: 'POST',
                data: {
                    name : $('#malls').val(),
                    description : $('#description').val(),
                    logo :  $('#logo').val(),
                    address_id :  $('#address_id').val(),
                    mall_id :  $('#mall_id').val(),
                    contact_id :  $('#contact_id').val(),
                    postal : $('#postal').val(),
                    unit_no : $('#unit_no').val(),
                    street : $('#street').val(),
                    country : $('#country').val(),
                    city : $('#city').val(),
                    phone_no : $('#phone_no').val(),
                    fax : $('#fax').val(),
                    email : $('#email').val(),
                    website : $('#website').val(),
                    opening_hours : $('#opening_hours').val(),
                    parking_hours : $('#parking_hours').val(),
                    services : $('#services').val(),
                    concierge : $('#concierge').val(),
                    latitude : $('#latitude').val(),
                    longitude : $('#longitude').val(),
                    radius : $('#radius').val(),
                    given_name : $('#given_name').val(),
                    family_name : $('#family_name').val(),
                    contact_email : $('#contact_email').val(),
                    contact_phone : $('#contact_phone').val(),
                },
                success: function(data){
                    if(data.status == 'NOK') {
                        $('#error').html(data.message).show();
                        $('#success').hide();    
                    } else {
                        $('#success').html(data.message).show();
                        $('#error').hide();
                        window.location.href='$redirectUrl';
                    }
                }
            });
        }, 2000);           
    }
JS;
$this->registerJs($script, View::POS_HEAD);
$this->registerCssFile("@web/js/chosen/chosen.min.css");
$this->registerCssFile("@web/css/jquery-ui.min.css");
?>
<div id="success" class="alert alert-success" role="alert" style="display:none">
</div>
<div id="error" class="alert alert-danger" role="alert" style="display:none">
</div>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Edit Malls</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Mall Details</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="malls" class=" form-control-label">*Name</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="malls" name="malls" value="<?= $malls['name']; ?>" placeholder="Enter Name" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">*Description</label></div>
                            <div class="col-12 col-md-9"><textarea name="description" id="description" rows="9" placeholder="Enter Description" value="<?= $malls['description']; ?>" class="form-control"><?= $malls['description']; ?></textarea></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="logo" class=" form-control-label">Logo</label></div>
                            <div class="col-12 col-md-9">
                                <form method="post" id="fileinfo" name="fileinfo" action="" enctype="multipart/form-data">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" multiple class="custom-file-input" name="filesToUpload[]" id="filesToUpload" onChange="processUpload();">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose files</label>
                                        </div>
                                        <?= Html::img('@web/img/loading.gif', ['alt'=>'Logo', 'id'=>'loadingmessage', 'style' => 'max-height: 10px; max-width: 10px; display:none']); ?>
                                    </div>
                                </form>
                                <br>
                                <div id="fileList">No Files Selected</div>
                                <br>
                                <?php
                                    $logos = explode(',',$malls['logo']);
                                    foreach($logos as $logo) {
                                        echo Html::img('@web/uploads/'.$logo, ['alt'=>'Logo', 'style'=>'max-height: 100px; max-width: 100px;']);
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label"></label></div>
                            <div class="col-12 col-md-9">
                                Image size should be at least 512 x 512 pixels<br>
                                Supported formats PNG, JPEG, GIF
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>   
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Mall Contact</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="postal" class=" form-control-label">*Postal Code</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="postal" name="postal" value="<?= $malls['postal_code']; ?>" placeholder="Enter Postal Code" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="unit_no" class=" form-control-label">Unit No</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="unit_no" name="unit_no" value="<?= $malls['unit_no']; ?>" placeholder="Enter Unit No" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="street" class=" form-control-label">Street Name</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="street" name="street" value="<?= $malls['street']; ?>" placeholder="Enter Street Name" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="country" class=" form-control-label">Country</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select a Country" id="country" name="country" class="standardSelect" tabindex="1">
                                    <option value=""></option>
                                    <?php
                                    $selected = '';
                                    foreach ($countries as $no => $data) {
                                        if($data['id'] == $malls['country_id']) $selected = 'selected';
                                        else $selected = '';
                                        echo '<option '.$selected.' value="' . $data['id'] . '">' . $data['country'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="city" class=" form-control-label">City</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Select a City" id="city" name="city" class="standardSelect" tabindex="1">
                                    <option value=""></option>
                                    <?php
                                    $selected = '';
                                    foreach ($cities as $no => $data) {
                                        if($data['id'] == $malls['city_id']) $selected = 'selected';
                                        else $selected = '';
                                        echo '<option '.$selected.' value="' . $data['id'] . '">' . $data['city'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="phone_no" class=" form-control-label">Phone No</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="phone_no" name="phone_no" value="<?= $malls['phone_no']; ?>" placeholder="Enter Phone No" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="fax" class=" form-control-label">Fax</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="fax" name="fax" value="<?= $malls['fax']; ?>" placeholder="Enter Fax" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="email" class=" form-control-label">Email</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="email" name="email" value="<?= $malls['email']; ?>" placeholder="Enter Email" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="website" class=" form-control-label">Website</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="website" name="website" value="<?= $malls['website']; ?>" placeholder="Enter Website" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>General Information</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="opening_hours" class=" form-control-label">Opening Hours</label></div>
                            <div class="col-12 col-md-9">
                                <textarea name="opening_hours" id="opening_hours" rows="3" placeholder="Enter Description" value="<?= $malls['opening_hours']; ?>" class="form-control"><?= $malls['opening_hours']; ?></textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="parking_hours" class=" form-control-label">Parking Hours</label></div>
                            <div class="col-12 col-md-9">
                                <textarea name="parking_hours" id="parking_hours" rows="3" placeholder="Enter Description" value="<?= $malls['parking_hours']; ?>" class="form-control"><?= $malls['parking_hours']; ?></textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="services" class=" form-control-label">Services Hours</label></div>
                            <div class="col-12 col-md-9">
                                <textarea name="services" id="services" rows="3" placeholder="Enter Description" value="<?= $malls['services']; ?>" class="form-control"><?= $malls['services']; ?></textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="concierge" class=" form-control-label">Concierge Location</label></div>
                            <div class="col-12 col-md-9">
                                <textarea name="concierge" id="concierge" rows="3" placeholder="Enter Description" value="<?= $malls['concierge']; ?>" class="form-control"><?= $malls['concierge']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" onclick="editMalls();" class="btn btn-primary btn-sm pull-right">
                            Submit
                        </button>
                    </div>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Contact Person</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="given_name" class=" form-control-label">Given Name</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="given_name" name="given_name" value="<?= $malls['given_name']; ?>" placeholder="Enter Given Name" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="family_name" class=" form-control-label">Family Name</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="family_name" name="family_name" value="<?= $malls['family_name']; ?>" placeholder="Enter Family Name" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="contact_email" class=" form-control-label">Email</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="contact_email" name="contact_email" value="<?= $malls['contact_email']; ?>" placeholder="Enter Email" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="contact_phone" class=" form-control-label">Phone No</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="contact_phone" name="contact_phone" value="<?= $malls['contact_phone']; ?>" placeholder="Enter Phone No" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <strong>Geofence Details</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="latitude" class=" form-control-label">*Latitude</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="latitude" name="latitude" value="<?= $malls['latitude']; ?>" placeholder="Enter Latitude" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="longitude" class=" form-control-label">*Longitude</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="longitude" name="longitude" value="<?= $malls['longitude']; ?>" placeholder="Enter Longitude" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="radius" class=" form-control-label">*Maximum Radius</label></div>
                            <div class="col-12 col-md-9">
                                <input type="number" id="radius" name="radius" value="<?= $malls['radius']; ?>" placeholder="Enter Radius" class="form-control" min="1" max="1000">
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-lg-4">
                <!-- NA -->
            </div> 
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<input type="hidden" id="logo" name="logo" value="<?= $malls['logo']; ?>">
<input type="hidden" id="mall_id" name="mall_id" value="<?= $malls['mall_id']; ?>">
<input type="hidden" id="address_id" name="address_id" value="<?= $malls['address_id']; ?>">
<input type="hidden" id="contact_id" name="contact_id" value="<?= $malls['contact_id']; ?>">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $("#postal").on('change keyup paste', function () {
        var source = [];
        var innerData = [];
        $.ajax({
            url: 'https://developers.onemap.sg/commonapi/search?searchVal=' + $("#postal").val() + '&returnGeom=Y&getAddrDetails=Y&pageNum=1',
            success: function (data) {
                $.each(data.results, function (index, value) {
                    source.push(value.POSTAL);
                    innerData.push(value);
                });
            }});
        $("#postal").autocomplete({
            source: source,
            focus: function (event, ui) {
                $(".ui-helper-hidden-accessible").hide();
                event.preventDefault();
            },
            messages: {
                noResults: '',
                results: function () {}
            },
            select: function (event, ui) {
                var postal = ui.item.value;
                $.each(innerData, function (index, value) {
                    if (value.POSTAL == postal) {
                        $("#latitude").val(value.LATITUDE);
                        $("#longitude").val(value.LONGITUDE);
                        $("#street").val(value.BLK_NO + ' ' + value.ROAD_NAME);
                        $("#country").val('187');
                        $('#country').trigger('chosen:updated');
                        $('#city').append('<option value="1">Singapore</option>');
                        $("#city").val('1');
                        $('#city').trigger("chosen:updated");
                        $("#phone_no").val('+65');
                    }
                });
            }
        });
    });
    
    function processUpload() {
        var input = document.getElementById("filesToUpload");
        var ul = document.getElementById("fileList");
        while (ul.hasChildNodes()) {
            ul.removeChild(ul.firstChild);
        }
        for (var i = 0; i < input.files.length; i++) {
            var li = document.createElement("li");
            if(isFileImage(input.files[i].type) === false) {
                li.innerHTML = "<span style='color:red'>"+input.files[i].name+" not supported format.</span>";
                ul.appendChild(li);
                break;
            }
            li.innerHTML = input.files[i].name;
            ul.appendChild(li);
        }
        if(!ul.hasChildNodes()) {
            var li = document.createElement("li");
            li.innerHTML = 'No Files Selected';
            ul.appendChild(li);
        }
    }
    
    function upload() {
        $('#loadingmessage').show();
        var url = '<?=$apiUrl.'?action=uploadImages&token='.$apiToken?>';
        var fd = new FormData(document.getElementById("fileinfo"));
        var input = document.getElementById("filesToUpload");
        if(input.files.length < 1) {
            $('#fileList').html("<span style='color:red'>Please select an image.</span>");
            $('#loadingmessage').hide();
        } else {
            $.ajax(url, {
                type: 'POST',  // http method
                data : fd,
                processData: false,  
                contentType: false,  
                enctype: 'multipart/form-data',
                success: function (r) {
                    $('#loadingmessage').hide();
                    if($('#logo').val() != '') {
                        var logoList = $('#logo').val() + ',' + r.data.file.join();
                        $('#logo').val(logoList);
                    } else {
                        $('#logo').val(r.data.file.join());
                    }
                    var ul = document.getElementById("fileList");
                    while (ul.hasChildNodes()) {
                        ul.removeChild(ul.firstChild);
                    }
                    for (var i = 0; i < input.files.length; i++) {
                        var li = document.createElement("li");
                        li.innerHTML = "<span style='color:green'>"+input.files[i].name+" uploaded.</span>";
                        ul.appendChild(li);
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    alert('error');
                }
            });
        }
    }
    
    function isFileImage(type) {
        const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
        return acceptedImageTypes.includes(type);
    }
    
    function isAllowedImageSize(height, width) {
        if(height < 512 || width < 512) {
            return false;
        } 
        return true;
    }
</script>
<?php
$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);
//$this->registerJsFile("@web/js/main.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/chosen/chosen.jquery.min.js", ['position' => View::POS_HEAD]);

?>