<?php
/* @var $this yii\web\View */

//use Yii;
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Edit Malls';
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">VIew Malls</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong><?= $malls['name']; ?></strong>
                    </div>
                    <div class="card-body card-block">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="home" aria-selected="true">View Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="map-tab" data-toggle="tab" href="#map" role="tab" aria-controls="profile" aria-selected="false">Map</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact Us</a>
                            </li>
                        </ul>
                        <div class="tab-content pl-3 p-1" id="myTabContent">
                            <div class="tab-pane fade active show" id="details" role="tabpanel" aria-labelledby="details-tab">
                                <div class="col-lg-4">
                                    <ul id="imageGallery">
                                        <?php
                                        $logos = explode(',',$malls['logo']);
                                        foreach($logos as $logo) {
                                            $url = Yii::getAlias('@web').'/uploads/'.$logo;
                                            echo '<li data-thumb="'.$url.'" data-src="'.$url.'">';
                                            echo Html::img('@web/uploads/'.$logo, ['alt'=>'Gallery']);
                                            echo '</li>';
                                        }
                                        ?>
                                    </ul>
                                    <br>
                                    <div class="row form-group">
                                        <div class="col-12"><?= $malls['description']; ?></div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card-body card-block">
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="postal" class=" form-control-label">Postal Code</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['postal_code']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="unit_no" class=" form-control-label">Unit No</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['unit_no']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="street" class=" form-control-label">Street Name</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['street']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="country" class=" form-control-label">Country</label></div>
                                            <div class="col-12 col-md-9">
                                                <?php
                                                    $selected = '';
                                                    foreach ($countries as $no => $data) {
                                                        if($data['id'] == $malls['country_id']) echo $data['country'];
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="city" class=" form-control-label">City</label></div>
                                            <div class="col-12 col-md-9">
                                                <?php
                                                    $selected = '';
                                                    foreach ($cities as $no => $data) {
                                                        if($data['id'] == $malls['city_id']) echo $data['city'];
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="phone_no" class=" form-control-label">Phone No</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['phone_no']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="fax" class=" form-control-label">Fax</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['fax']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="email" class=" form-control-label">Email</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['email']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="website" class=" form-control-label">Website</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['website']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card-body card-block">
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="opening_hours" class=" form-control-label">Opening Hours</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['opening_hours']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="parking_hours" class=" form-control-label">Parking Hours</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['parking_hours']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="services" class=" form-control-label">Services Hours</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['services']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="concierge" class=" form-control-label">Concierge Location</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['concierge']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
                                <div class="col-lg-4">
                                    <?php
                                        $url = 'https://developers.onemap.sg/commonapi/staticmap/getStaticImage?layerchosen=night&lat='.$malls['latitude'].'&lng='.$malls['longitude'].'&zoom=17&height=512&width=512';
                                        echo Html::img($url, ['alt'=>'Map']);
                                    ?>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card-body card-block">
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="latitude" class=" form-control-label">Latitude</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['latitude']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="longitude" class=" form-control-label">Longitude</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['longitude']; ?>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="radius" class=" form-control-label">Maximum Radius</label></div>
                                            <div class="col-12 col-md-9">
                                                <?= $malls['radius']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="card-body card-block">
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="given_name" class=" form-control-label">Given Name</label></div>
                                        <div class="col-12 col-md-9">
                                            <?= $malls['given_name']; ?>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="family_name" class=" form-control-label">Family Name</label></div>
                                        <div class="col-12 col-md-9">
                                            <?= $malls['family_name']; ?>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="contact_email" class=" form-control-label">Email</label></div>
                                        <div class="col-12 col-md-9">
                                            <?= $malls['contact_email']; ?>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="contact_phone" class=" form-control-label">Phone No</label></div>
                                        <div class="col-12 col-md-9">
                                            <?= $malls['contact_phone']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<?php
$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/lightslider.js", ['position' => View::POS_HEAD]);
$this->registerCssFile("@web/css/lightslider.css");
?>
<script>
$(document).ready(function() {
    /*$('#imageGallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:5,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide'
            });
        }   
    });*/
    
    $(document).ready(function() {
        $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            vertical:true,
            verticalHeight:295,
            vThumbWidth:50,
            thumbItem:8,
            thumbMargin:4,
            slideMargin:0
        });  
    });
});
</script>