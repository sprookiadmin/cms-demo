<?php
/* @var $this yii\web\View */

//use Yii;
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Edit Notification';
$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);
//$this->registerJsFile("@web/js/main.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/chosen/chosen.jquery.min.js", ['position' => View::POS_HEAD]);

$script = <<< JS
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    });
JS;
$this->registerJs($script, View::POS_HEAD);
$notificationUrl = $apiUrl . '?action=updateNotification&token=' . $apiToken;
$notificationId = $notificationData["id"];
$redirectUrl = Yii::$app->getUrlManager()->getBaseUrl() . '/admin/notification-list';
$script = <<< JS
    function updateNotification() {
        var data = $('#beacon').val().split(",");
        $.ajax({
            url: '$notificationUrl',
            dataType: 'json',
            type: 'POST',
            data: {
                notificationId : '$notificationId',
                userId : '$user->id',
                notification : $('#notification').val(),
                beacon_id : data[0],
                proximity : data[1],
                major : data[2],
                minor : data[3],
                beaconGroupId: $('#beaconGroup').val()
            },
            success: function(data){
                if(data.status == 'NOK') {
                    $('#error').html(data.message).show();
                    $('#success').hide();    
                } else {
                    $('#success').html(data.message).show();
                    $('#error').hide();
                    window.location.href='$redirectUrl';
                }
            }
        });
    }
JS;
$this->registerJs($script, View::POS_END);
$this->registerCssFile("@web/js/chosen/chosen.min.css");
?>
<div id="success" class="alert alert-success" role="alert" style="display:none">
</div>
<div id="error" class="alert alert-danger" role="alert" style="display:none">
</div>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Edit Notification</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Edit Notification</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="notification" class=" form-control-label">Notification</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="notification" name="notification" value="<?= $notificationData['notification']; ?>" placeholder="Enter Notification" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="beacon" class=" form-control-label">Beacon</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Choose a Beacon" id="beacon" name="beacon" class="standardSelect" tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($beaconData as $no => $beacon) {
                                            $selected = '';
                                            if($notificationData['beacon_id'] == $beacon['uniqueId']) {
                                                $selected = 'selected';
                                            }
                                            echo '<option value="'.$beacon['uniqueId'].','.$beacon['proximity'].','.$beacon['major'].','.$beacon['minor'].'" '.$selected.'>'.$beacon['uniqueId'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="beaconGroup" class=" form-control-label">Beacon Group</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Choose a Beacon Group" id="beaconGroup" name="beaconGroup" class="standardSelect" tabindex="1">
                                    <option value=""></option>
                                    <?php
                                        foreach($beaconGroupData as $id => $beaconGroup) {
                                            $selected = '';
                                            if($notificationData['beacon_group_id'] == $id) {
                                                $selected = 'selected';
                                            }
                                            echo '<option value="'.$id.'"'.$selected.'>'.$beaconGroup.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" onclick="updateNotification();" class="btn btn-success btn-sm">Submit</button>
                    </div>
                </div>
            </div>            
        </div>
    </div><!-- .animated -->
</div><!-- .content -->