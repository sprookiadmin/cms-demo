<?php
use frontend\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \yii\web\View;

$this->title = 'Manage API';
$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);
?>
<script type="text/javascript">
    function updateApiKey() {
        $.ajax({
            url: '<?= $apiUrl . '?action=updateApiKey&token=' . $apiToken; ?>',
            dataType: 'json',
            type: 'POST',
            data: {
                userId : '<?= $user->id; ?>',
                apiKey : $('#apiKey').val(),
            },
            success: function(data){
                alert(data.message);
            }
        });
    }
</script>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Account Management</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/admin/index'; ?>">Dashboard</a></li>
                    <li class="active">Security</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">    
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Server API Key</strong>
                    </div>
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="apiKey" class=" form-control-label">API Key</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" maxlength="32" id="apiKey" name="apiKey" value="<?php echo $apiKey; ?>" placeholder="Enter API Key" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" onclick="updateApiKey();" class="btn btn-success btn-sm">Submit</button>
                    </div>
                </div>
            </div>
<?php /*           <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <strong>Change Password</strong>
                    </div>
                    <?php $form = ActiveForm::begin(); ?>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="currentPassword" class=" form-control-label">Current Password</label></div>
                                <div class="col-12 col-md-9">
                                    <?= $form->field($user, 'currentPassword')->passwordInput(['class' => 'form-control', 'placeholder' => 'Enter Current Password'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="newPassword" class=" form-control-label">New Password</label></div>
                                <div class="col-12 col-md-9">
                                    <?= $form->field($user, 'newPassword')->passwordInput(['class' => 'form-control', 'placeholder' => 'Enter New Password'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="newPasswordConfirm" class=" form-control-label">New Password Confirm</label></div>
                                <div class="col-12 col-md-9">
                                    <?= $form->field($user, 'newPasswordConfirm')->passwordInput(['class' => 'form-control', 'placeholder' => 'Enter New Password Confirm'])->label(false); ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-sm">
                                Submit
                            </button>
                        </div>
                    <?php ActiveForm::end();?>
                </div>
            </div> */ ?>
        </div>
    </div>
</div>


<?php
/*$script = <<< JS
    function updateApiKey() {
        $.ajax({
            url: $('#apiUrl').val(),
            dataType: 'json',
            type: 'POST',
            data: JSON.stringify( { "userId": $('#userId').val(), "apiKey": $('#apiKey').val() } ),
            success: function(data){
                alert(data.message);
            },
        });
    }
JS;
$this->registerJs($script,\yii\web\View::POS_HEAD);*/
?>  