<?php

/* @var $this \yii\web\View */
/* @var $content string */

use Yii;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \yii\web\View;

AppAsset::register($this);
$this->beginPage() 
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?= Yii::$app->language ?>">
<!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="Admin Dashboard - Beacons">
    
    <?php
        $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'sizes' => '180x180', 'href' => '/images/favicon.png']);
        $this->registerCssFile("@web/css/bootstrap/dist/css/bootstrap.min.css");
        $this->registerCssFile("@web/css/font-awesome/css/font-awesome.min.css");
        $this->registerCssFile("@web/css/themify-icons/css/themify-icons.css");
        $this->registerCssFile("@web/css/flag-icon-css/css/flag-icon.min.css");
        $this->registerCssFile("@web/css/selectFX/css/cs-skin-elastic.css");
        $this->registerCssFile("@web/css/jqvmap/dist/jqvmap.min.css");
        $this->registerCssFile("@web/css/admin-site.css");
    ?>
    <?php
        //$this->registerJsFile("@web/js/jquery/dist/jquery.min.js", ['position' => View::POS_HEAD]);
        //$this->registerJsFile("@web/js/bootstrap/dist/js/bootstrap.min.js", ['position' => View::POS_HEAD]);
        //$this->registerJsFile("@web/js/popper.js/dist/umd/popper.min.js", ['position' => View::POS_HEAD]);
    ?>
    <?php $this->registerCsrfMetaTags() ?>
    <title>Admin Dashboard</title>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <?= Html::img('@web/images/invigor-beta.jpg', ['alt'=>'Logo', 'class'=>'navbar-brand']); ?>
            <?= Html::img('@web/images/invigor-cms.png', ['alt'=>'Logo', 'class'=>'navbar-brand hidden']); ?>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/admin/index"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">Directory Management</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon ti-layout-tab"></i>Malls</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/malls-list'; ?>">Malls List</a></li>
                        <li><i class="menu-icon ti-plus"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/malls-add'; ?>">Add Malls</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon ti-layout-tab"></i>Merchants</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/merchants-list'; ?>">Merchants List</a></li>
                        <li><i class="menu-icon fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/merchants-list'; ?>">Merchants Category List</a></li>
                        <li><i class="menu-icon ti-plus"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/merchants-add'; ?>">Add Merchants</a></li>
                    </ul>
                </li>
                <h3 class="menu-title">Beacons Management</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Beacon</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/beacon-list'; ?>">Beacon List</a></li>
                        <li><i class="menu-icon ti-plus"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/beacon-add'; ?>">Add Beacon</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Beacon Group</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/beacon-group-list'; ?>">Beacon Group List</a></li>
                        <li><i class="menu-icon ti-plus"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/beacon-group-add'; ?>">Add Beacon Group</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Beacon Manufacturer</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/beacon-manufacturer-list'; ?>">Beacon Manufacturer List</a></li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Beacon Type</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/beacon-type-list'; ?>">Beacon Type List</a></li>
                    </ul>
                </li>
                
                <h3 class="menu-title">Gateway Management</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Gateway</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/gateway-list'; ?>">Gateway List</a></li>
                    </ul>
                </li>
                <h3 class="menu-title">Notification Management</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Notification</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-list-ol"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/notification-list'; ?>">Notification List</a></li>
                        <li><i class="menu-icon ti-plus"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/notification-add'; ?>">Add Notification</a></li>
                    </ul>
                </li>
                <h3 class="menu-title">Account Management</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Account</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-user-secret"></i><a href="<?= Yii::$app->getUrlManager()->getBaseUrl().'/admin/security'; ?>">Security</a></li>
                        <li><i class="menu-icon fa fa-address-card"></i><a href="#">Billing Address</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->

<!-- Right Panel -->

<div id="right-panel" class="right-panel">
    <!-- Header-->
    <header id="header" class="header">
        <div class="header-menu">
            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
            </div>
            <div class="col-sm-5">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php
                        if(isset(Yii::$app->user->identity->username)) {
                            echo Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')',
                                ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm();
                        }
                        ?>
                    </a>                    
                </div>
                <div class="language-select dropdown" id="language-select">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                        <i class="flag-icon flag-icon-us"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="language">
                        <div class="dropdown-item">
                            <span class="flag-icon flag-icon-fr"></span>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-es"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-us"></i>
                        </div>
                        <div class="dropdown-item">
                            <i class="flag-icon flag-icon-it"></i>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->

    <?= $content; ?>
</div><!-- /#right-panel -->

<!-- Right Panel -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
