<?php

/* @var $this yii\web\View */

$this->title = 'Invigor Beacon';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Invigor's Beacon Demo</h1>
        <p class="lead">Welcome to our demo application dashboard.</p>
        <?php 
            if (Yii::$app->user->isGuest) { 
                echo '<p><a class="btn btn-lg btn-success" href="/site/login">Get started</a></p>';
            } else {
                echo '<p><a class="btn btn-lg btn-success" href="/admin/index">Get started</a></p>';
            }
        ?>        
    </div>
</div>
